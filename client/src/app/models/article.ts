export interface Article{
    _id?:string;
    title?: string;
    objectID?: string;
    created_at?: any;
    url?: string;
    author?: string;
    points?: string;
    story_text?: string;
    comment_text?: string;
    num_comments?: string;
    story_id?: number;
    story_title?: string;
    story_url?: string;
    parent_id?: number;
    created_at_i?: number;
    _tags?: object;
    _highlightResult?: object;
}