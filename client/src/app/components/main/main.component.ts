import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../../services/article.service';
import { Article } from 'src/app/models/article';
import * as moment from 'moment';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  articleList: Article[] = [];


  constructor(private articleService: ArticleService) { }

  ngOnInit() {
    this.getArticleList();
  }
  getArticleList() {
    this.articleService.getArticleList()
      .subscribe(
        res => {
          this.articleList = res;
          for (let article of this.articleList) {
            article.created_at = moment(article.created_at).calendar();

          }
        },
        err => console.error(err)
      );
  }
  redirectTo(article: Article) {
    let URL = article.story_url || article.url;
    window.open(URL);
  }
  deleteArticle(article: Article) {
    this.articleService.deleteArticleList(article)
      .subscribe(
        res => {
          console.log(res);
          this.getArticleList();
        },
        err => console.error(err)
      )
  }
}
