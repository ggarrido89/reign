import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Article } from '../models/article';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  URL='http://localhost:3000/article';
  constructor(private http:HttpClient) { }

  getArticleList():Observable<Article[]>{
    return this.http.get<Article[]>(this.URL);
  }
  deleteArticleList(article:Article){
    return this.http.delete(`${this.URL}/${article._id}`);
  }
}
