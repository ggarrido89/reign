require('./background/background');
const express = require('express');
const morgan = require('morgan');
const cors=require('cors');
const app = express();
const { mongoose } = require('./database');

//SETTINGS
app.set('port', process.env.PORT || 3000);

//MIDLEWARES
app.use(morgan('dev'));
app.use(express.json());
app.use(cors({origin:'*'}));

//ROUTERS
app.use(require('./routers/articleRoutes'));

app.listen(app.get('port'), () => {
    console.log('Listening on ' + app.get('port'))
});