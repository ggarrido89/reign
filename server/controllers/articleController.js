
const Article = require('../models/articleModel');
const articleController = {};

articleController.getData = async (req, res) => {
    const articleList = await Article.find().sort({created_at:-1});
    res.json(articleList);
}
articleController.addData = async (req, res) => {
    const article = new Article(req.body);
    await article.save();
    res.json({ res: 'OK!' });
}
articleController.updData = async (req, res) => {
    const { id } = req.params;
    await Article.findByIdAndUpdate(id, {$set:req.body}, {new:true});

    res.json({ res: 'OK!' });
}
articleController.delData = async (req, res) => {
    // const { id } = req.params;
    await Article.findByIdAndRemove(req.params.id)
    res.json({ res: 'OK!' });
}

module.exports = articleController;