var Request = require("request");
var Article = require("../models/articleModel");

insertArticle();
setInterval(insertArticle,3600000);

function insertArticle() {
    Request.get("https://hn.algolia.com/api/v1/search_by_date?query=nodejs", async (error, response, body) => {
        let data = JSON.parse(body);
        let articleList=data.hits;
        for(let article of articleList){
            let data=new Article(article);
            await data.save();
        }
    });
}