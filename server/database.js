const mongoose = require('mongoose');

const URL = 'mongodb://mongo:27017/articles';

mongoose.connect(URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(db => console.log('Connected to mongo'))
    .catch(err => {console.error('DB Error:');console.error(err)})