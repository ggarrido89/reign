const mongoose = require('mongoose');
const { Schema } = mongoose;

const ArticleSchema = new Schema({
    objectID: { type: String, required: true },
    created_at: { type: Date, required: false },
    title: { type: String, required: false },
    url: { type: String, required: false },
    author: { type: String, required: false },
    points: { type: String, required: false },
    story_text: { type: String, required: false },
    comment_text: { type: String, required: false },
    num_comments: { type: String, required: false },
    story_id: { type: Number, required: false },
    story_title: { type: String, required: false },
    story_url: { type: String, required: false },
    parent_id: { type: Number, required: false },
    created_at_i: { type: Number, required: false },
    _tags: { type: Object, required: false },
    _highlightResult: { type: Object, required: false },
})

module.exports = mongoose.model('Article', ArticleSchema);