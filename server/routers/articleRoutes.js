const express = require('express');
const articleController = require('../controllers/articleController')
const router = express.Router();

router.get('/article', articleController.getData);
router.post('/article', articleController.addData);
router.put('/article/:id', articleController.updData);
router.delete('/article/:id', articleController.delData);

module.exports = router;