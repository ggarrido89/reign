# Fullstack Developer Application - Gonzalo Garrido

You will find all job application requirements here.

## Content

* app/      # Angular v8 client side app code
* server/   # Express JS server side app code
* data/     # stack.yml file to load images using docker-compose

## Docker Images
* ggarrido89/reign-app      # Client app image. It uses port 80
* ggarrido89/reign-server   # Server app image. It uses port 3000
* mongo                     # Oficial mongodb docker image. It uses port 27017

### Execution 🔧

Execute command:

```
cd data
docker-compose -f stack.yml up
```

Go to web browser and open:

```
http://localhost
```

## Builded with

* Angular/cli v8
* Node JS / Express
* MongoDB
* Docker Compose